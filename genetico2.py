import numpy as np
import gym
import random
import matplotlib.pyplot as plt

n_ep = 500
constvar = 0.2
pmutacao = 2.5
constmut = 0.02

class Individuo:
	def __init__(self, gerados, const=[], primeiraAcao = []):
		self.nome = gerados
		if const == []:
			self.const = [float(random.uniform(-constvar,constvar)) for i in range(4)]
		else:
			self.const = const
		self.recompensa = 0
		self.acoes=[]
		if primeiraAcao == []:
			self.acoes.append([random.uniform(-1,1) for i in range(4)])
			self.calcAcoes()
		else:
			self.acoes.append(primeiraAcao)

	def calcAcoes(self):
		while len(self.acoes)<n_ep:
			valor = [self.acoes[-1][0], self.acoes[-1][1], self.acoes[-1][2], self.acoes[-1][3]]
			for i in range(4):
				if valor[i] + self.const[i] >1 or valor[i] + self.const[i] <-1:
					self.const[i]*=-1
				valor[i] += self.const[i]
			self.acoes.append(valor)


	def reset(self):
		self.recompensa = 0

	def mutacao(self):		
		for i in range(4):
			if random.uniform(0,100) <= pmutacao:
				self.const[i] += float(random.uniform(-constmut,constmut))

	def crossover(self, another, gerados):
		consfilho1 = []
		consfilho2 = []
		acao1 = []
		acao2 = []

		for i in range(4):
			a = np.random.choice([1 , 2])
			if a ==1:
				consfilho1.append(self.const[i])
				acao1.append(self.acoes[0][i])
			else:
				consfilho1.append(another.const[i])
				acao1.append(another.acoes[0][i])

			a = np.random.choice([1 , 2])
			if a ==1:
				consfilho2.append(self.const[i])
				acao2.append(self.acoes[0][i])
			else:
				consfilho2.append(another.const[i])
				acao2.append(another.acoes[0][i])

		filho1 = Individuo(gerados , consfilho1, acao1)
		filho2 = Individuo(gerados + 1, consfilho2, acao2)

		filho1.mutacao()
		filho2.mutacao()

		filho1.calcAcoes()
		filho2.calcAcoes()

		return filho1, filho2


class Geracao:
	def __init__(self, ni = 100, env = None, n = 100):
		self.gerados = 1
		self.geracao = 0
		self.ni = ni
		self.n = n
		self.individuos = []
		for i in range(self.ni):
			self.individuos.append(Individuo(self.gerados))
			self.gerados +=1

		if env is None:
			self.env = gym.make('BipedalWalker-v2')
		else:
			self.env = env 

	def selecao(self):
		aux = []
		p = np.array([i.recompensa for i in self.individuos])
		minimo = abs(np.amin(p)) +1
		p += minimo
		soma = p.sum()
		p /= soma
		selecionados_sobreviver = sorted(self.individuos, key=lambda x: x.recompensa, reverse=True)

		for i in range(10):
			aux.append(selecionados_sobreviver[i])

		while (len(aux) < self.n):
			par = np.random.choice(self.individuos, size=2, replace=False, p=p)
			s3, s4 = par[0].crossover(par[1], self.gerados)
			self.gerados += 2
			aux.append(s3)
			aux.append(s4)

		self.individuos = aux

	def reset(self):
		for i in self.individuos:
			i.reset()

	def passo_simulacao(self, n = n_ep):
		melhor=self.individuos[0]
		for i in self.individuos:  
			observation = self.env.reset()
			for t in range(n):
				observation, reward, done, info = self.env.step(i.acoes[t])
				i.recompensa += reward
				if done: break
			self.env.close()
			if i.recompensa > melhor.recompensa:
				melhor = i

		self.geracao += 1
		print('Melhor recompensa =', melhor.recompensa, '  ', melhor.nome)
		return melhor.recompensa

	def simulacao(self):
		melhores=[]
		for i in range(self.n):
			print('geracao ', i+1)
			self.reset()
			melhores.append(self.passo_simulacao())
			self.selecao()
		y = [i for i in melhores]
		x = [i for i in range(len(melhores))]
		plt.xlabel('Geracao')
		plt.ylabel('Recompensa')
		plt.title('Genetico')
		plt.plot(x,y)
		plt.show()
		'''for i in range(0,len(melhores)-1,5):
			print('melhor da geracao ', i+1)
			observation = self.env.reset()	
			for t in range(n_ep):
				self.env.render()
				observation, reward, done, info = self.env.step(melhores[i].acoes[t])
				if done: break'''

g = Geracao()
g.simulacao()
































import numpy as np
import random
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.metrics import f1_score
from sklearn.preprocessing import StandardScaler

pmutacaoTotal = 10
maxnNeuronios = 300
maxnLayers = 10
minAlpha = 1e-10
maxAlpha = 1e-3
possibleActivations = ['relu','identity','logistic','tanh']

class Individuo:
    def __init__(self, layers, alpha, activation):
        self.layers = [i for i in layers]
        self.alpha = alpha
        self.activation = activation
        self.recompensa = 0

    def setCoefs(self, coefs):
        self.coefs = coefs

    def setBias(self, bias):
        self.bias = bias

    def reset(self):
        self.recompensa = 0

    def mutacao(self):
        x = random.uniform(0,100)
        if x<=pmutacaoTotal/5:
            del self.layers[-1]

        x = random.uniform(0,100)
        if x<=pmutacaoTotal/5:
            self.layers.append(random.randint(2,maxnNeuronios))

        x = random.uniform(0,100)
        if x<=pmutacaoTotal/5:
            i = random.randint(0,len(self.layers)-1)
            self.layers[i] = random.randint(2,maxnNeuronios)

        x = random.uniform(0,100)
        if x<=pmutacaoTotal/5:
            self.alpha =  random.uniform(minAlpha, maxAlpha)

        x = random.uniform(0,100)
        if x<=pmutacaoTotal/5:
            self.activation = random.choice(possibleActivations)




    def crossover(self, another):
        filho1 = Individuo(self.layers , self.alpha, another.activation)
        filho2 = Individuo(another.layers, another.alpha, self.activation)

        filho1.mutacao()
        filho2.mutacao()

        return filho1, filho2


class Geracao:
    def __init__(self, ni = 20, n = 20):
        self.geracao = 0
        self.ni = ni
        self.n = n
        self.individuos = []
        for i in range(self.ni):
            layers = []
            nlayers = random.randint(2, maxnLayers)
            layers.append(random.randint(2, maxnNeuronios))
            for j in range(nlayers-1):
                if layers[-1] == 2: break
                layers.append(random.randint(2, layers[-1]))

            alpha = random.uniform(minAlpha, maxAlpha)
            activation = random.choice(possibleActivations)

            self.individuos.append(Individuo(layers, alpha, activation))


    def selecao(self):
        aux = []
        p = np.array([i.recompensa for i in self.individuos])
        minimo = abs(np.amin(p)) +1
        p += minimo
        soma = p.sum()
        p /= soma
        selecionados_sobreviver = sorted(self.individuos, key=lambda x: x.recompensa, reverse=True)

        
        aux.append(selecionados_sobreviver[0])
        aux.append(selecionados_sobreviver[1])

        while (len(aux) < self.n):
            par = np.random.choice(self.individuos, size=2, replace=False, p=p)
            s3, s4 = par[0].crossover(par[1])
            aux.append(s3)
            aux.append(s4)

        self.individuos = aux

    def reset(self):
        for i in self.individuos:
            i.reset()

    def passo_simulacao(self):
        melhor=self.individuos[0]

        x_train, x_test, y_train, y_test = train_test_split(xtr, ytr, test_size=0.2, random_state=4)
        y_train = y_train.reshape(1,-1)[0]
        y_test = y_test.reshape(1,-1)[0]

        scaler = StandardScaler()
        scaler.fit(x_train)
        x_train = scaler.transform(x_train)
        x_test = scaler.transform(x_test)
        for i in self.individuos:
            rede = MLPClassifier(solver='lbfgs', alpha=i.alpha, hidden_layer_sizes=i.layers, activation=i.activation, random_state=1, max_iter=200)
            rede.fit(x_train, y_train)

            #avaliacao
            ytest_pred = rede.predict(x_test)
            i.setBias(rede.intercepts_)
            i.setCoefs(rede.coefs_)

            i.recompensa = f1_score(y_test,ytest_pred)
            if i.recompensa > melhor.recompensa:
                melhor = i

        self.geracao += 1
        print('Melhor recompensa =', melhor.recompensa)
        return melhor

    def simulacao(self):
        melhores=[]
        for i in range(self.n):
            print('geracao ', i+1)
            self.reset()
            melhores.append(self.passo_simulacao())
            self.selecao()
        #y = [i for i in melhores]
        y = []
        for ind in melhores:
            y.append(ind.recompensa)
        x = [i for i in range(len(melhores))]
        plt.xlabel('Geracao')
        plt.ylabel('Recompensa')
        plt.title('Genetico')
        plt.plot(x,y)
        plt.show()

        return melhores[-1]
        

prepastas="./websocket/"
#prepastas="./"

filename = prepastas+'dados-q70-d30000.csv.gz'
filename2 = prepastas+'rotulos-q70-d30000.csv.gz'
xtr = pd.read_csv(filename, compression='gzip', header=0, sep=',', quotechar='"').values
ytr = pd.read_csv(filename2, compression='gzip', header=0, sep=',', quotechar='"').values

g = Geracao(ni=50, n=15)
melhor = g.simulacao()
print(melhor.layers)
print(melhor.activation)
print(melhor.alpha)
